# lambda-cs-connection-detection

lambda to detect codestar connection use and trigger pipeline with calling repository

## Prerequisites
* Terraform v0.12+
* cdktf
* Node.js v12.16+
* Python v3.7+ and pipenv
* AWS account and AWS Access Credentials


From a directory with these subdirectories:
( heq-Terragrunt-Infra, heq-Terragrunt-Modules, lambda-cs-connection-detection all need to be at the same level in the dir tree )

```bash
$ ls -l .
drwxr-xr-x  10 jnorment  staff  320 Jul 12 16:28 heq-Terragrunt-Infra
drwxr-xr-x   9 jnorment  staff  288 Jul 12 12:25 heq-Terragrunt-Modules
drwxr-xr-x   6 jnorment  staff  192 Jul 22 16:52 lambda-cs-connection-detection
```

This will mount useful repos into the container for development:

```bash
docker run --rm -it -v `pwd`/heq-Terragrunt-Infra:/tg-infra -v `pwd`/heq-Terragrunt-Modules:/heq-Terragrunt-Modules -v `pwd`/lambda-cs-connection-detection:/cdktg -v ~/.aws:/root/.aws cdktg sh
```


## Installation - Docker:
( lambda-cs-connection-detection, below, refers to this repo ):
```bash
git clone https://gitlab.com/cognitiaclaeves/dkr-cdktf.git && cd dkr-cdktf
dkr-cdktf $ docker build -t cdktg .
docker run --rm -it -v `pwd`/lambda-cs-connection-detection:/cdktg -v ~/.aws:/root/.aws cdktg sh
# pipenv install
# pipenv shell
```

## Run - Docker:
```bash
# pipenv shell
# export TF_STATE_BUCKET_NAME=<name of s3 bucket where state file will be created>
# AWS_DEFAULT_REGION=us-east-1 AWS_PROFILE=preopi-admin cdktf diff
```


Debugging:

Environment variables:

`TF_LOG` - [Terraform log level](https://www.terraform.io/docs/internals/debugging.html): `DEBUG`,  `INFO`, `WARN`, `ERROR`, `JSON`
`CDKTF_LOG_LEVEL` - `DEBUG`

`cdktf synth` generates cdktf.out/**/cdk.tf.json. `cd`ing into the directory with that file, you can then run `terraform apply` to see how terraform would process the file outside of CDKTF.

example:
`cdktf synth`

to diff:
`AWS_PROFILE=preopi-admin AWS_DEFAULT_REGION=us-east-1 cdktf diff`

to see the plan after generation:
```bash
ln -s cdktf.out/stacks/lambda-example out-lambda-example
cd out-lambda-example
AWS_PROFILE=preopi-admin AWS_DEFAULT_REGION=us-east-1 terraform show plan
```
