# CDKTF-lambda-example
> Example how to deploy AWS lambda with CDKTF

## Prerequisites
* Terraform v0.12+
* cdktf
* Node.js v12.16+
* Python v3.7+ and pipenv
* AWS account and AWS Access Credentials


## Installation - Docker:
( lambda-cs-connection-detection, below, refers to this repo ):
```bash
git clone https://gitlab.com/cognitiaclaeves/dkr-cdktf.git && cd dkr-cdktf
dkr-cdktf $ docker build -t cdktg .
docker run --rm -it -v `pwd`/lambda-cs-connection-detection:/cdktg -v ~/.aws:/root/.aws cdktg sh
# pipenv install
# pipenv shell
```

## Run - Docker:

It's worth mentioning that CDKTF defaults to using Terraform Cloud. This project is configured to use S3.
It does this in code:

```python
    # configure TF backend to use S3 to store state file
    stack.add_override(
        "terraform.backend",
        {
            "s3": {
                "bucket": tf_bucket_name,
                "key": "terraform-state/{acct}/{env}/lambda/{lambda_prj}".format(**subst_vars),
                "region": region,
                "encrypt": True,
            }
        },
    )
```
... hence, the env variable TF_STATE_BUCKET_NAME needs to be set before running:

```bash
# pipenv shell
# export TF_STATE_BUCKET_NAME=<name of s3 bucket where state file will be created>
# AWS_DEFAULT_REGION=us-east-1 AWS_PROFILE=preopi-admin cdktf diff
```

---
Note:
Original project (https://github.com/NursultanBeken/cdktf-ex/tree/main/lambda-example) used OSX (brew) and pipenv.
I consider the Docker environment to be a preferred approach. ( And where I ultimately want the project I have in mind to go. )
I'm not paying very close attention to anything below.


## Usage
```bash
git clone https://github.com/NursultanBeken/cdktf-ex.git
cd lambda-example
export AWS_DEFAULT_REGION=<your region>
export TF_STATE_BUCKET_NAME=<name of s3 bucket where state file will be created>
pipenv install
make build
make deploy
```

## Cleanup
```bash
make clean
```

<!--
My Notes

`make build` always executes a `cdktf get` ... which takes a long time if the aws provider is included.
An alternative is to install `cdktf-cdktf-provider-aws` into environment (see Pipfile)

See main.py for more notes.
-->